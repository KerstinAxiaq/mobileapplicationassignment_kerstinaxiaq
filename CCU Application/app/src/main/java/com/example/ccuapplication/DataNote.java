package com.example.ccuapplication;

public class DataNote {
    String name;
    String symbol;
    String id;

    public DataNote(String id,String name, String symbol)
    {
        this.name = name;
        this.symbol = symbol;
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public String getSymbol()
    {
        return symbol;
    }

    public String getId()
    {
        return id;
    }
}
