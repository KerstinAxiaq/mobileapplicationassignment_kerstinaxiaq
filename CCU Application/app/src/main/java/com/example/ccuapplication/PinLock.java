package com.example.ccuapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.andrognito.pinlockview.IndicatorDots;
import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;

public class PinLock extends AppCompatActivity {

        private PinLockView mPinLockView;
        private IndicatorDots mIndicatorDots;
        private final static String TAG = PinLock.class.getSimpleName();
        private final static String TRUE_CODE = "123";

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_pin_lock);

            setContentView(R.layout.activity_pin_lock);

            mPinLockView = (PinLockView) findViewById(R.id.pin_lock_view);
            mIndicatorDots = (IndicatorDots) findViewById(R.id.indicator_dots);

            //attach lock view with dot indicator
            mPinLockView.attachIndicatorDots(mIndicatorDots);

            mPinLockView.setPinLength(3);

            mPinLockView.setPinLockListener(new PinLockListener() {
                @Override
                public void onComplete(String pin) {
                    Log.d(TAG, "lock code: " + pin);

                    if (pin.equals(TRUE_CODE)) {
                        Intent intent = new Intent(PinLock.this, LoggedIn.class);
                        intent.putExtra("code", pin);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(PinLock.this, "Failed code, try again!", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onEmpty() {
                    Log.d(TAG, "lock code is empty!");
                }

                @Override
                public void onPinChange(int pinLength, String intermediatePin) {
                    Log.d(TAG, "Pin changed, new length " + pinLength + " with intermediate pin " + intermediatePin);
                }
            });
        }
    }
