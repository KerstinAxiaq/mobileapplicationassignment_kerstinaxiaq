package com.example.ccuapplication;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment2 extends Fragment {


    public Fragment2() {
        // Required empty public constructor
    }

    private final String ASYNC_TASK_TAG = "ASYNC_TASK";

    private Button executeAsyncTaskButton;
    private Button cancelAsyncTaskButton;
    private ProgressBar asyncTaskProgressBar;
    private TextView asyncTaskLogTextView;

    private MyAsyncTask myAsyncTask;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fragment2, container, false);

        this.executeAsyncTaskButton = (Button)view.findViewById(R.id.executeAsyncTaskButton);
        this.executeAsyncTaskButton.setEnabled(true);

        this.cancelAsyncTaskButton = (Button)view.findViewById(R.id.cancelAsyncTaskButton);
        this.cancelAsyncTaskButton.setEnabled(false);

        this.asyncTaskProgressBar = (ProgressBar)view.findViewById(R.id.asyncTaskProgressBar);
        this.asyncTaskLogTextView = (TextView)view.findViewById(R.id.asyncTaskLogTextView);

        executeAsyncTaskButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myAsyncTask = new MyAsyncTask();
                myAsyncTask.execute(Integer.parseInt("10"));

                executeAsyncTaskButton.setEnabled(false);
                cancelAsyncTaskButton.setEnabled(true);
            }
        });

        cancelAsyncTaskButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myAsyncTask.cancel(true);
            }
        });
        return view;
    }

    private class MyAsyncTask extends AsyncTask<Integer, Integer, String> {

        protected void onPreExecute() {
            asyncTaskLogTextView.setText("Loading");
            Log.i(ASYNC_TASK_TAG, "onPreExecute() is executed.");
        }
        protected String doInBackground(Integer... inputParams) {

            StringBuffer retBuf = new StringBuffer();
            boolean loadComplete = false;

            try
            {
                Log.i(ASYNC_TASK_TAG, "doInBackground(" + inputParams[0] + ") is invoked.");

                int paramsLength = inputParams.length;
                if(paramsLength > 0) {
                    Integer totalNumber = inputParams[0];
                    int totalNumberInt = totalNumber.intValue();

                    for(int i=0;i < totalNumberInt; i++)
                    {
                        int progressValue = (i * 100 ) / totalNumberInt;
                        publishProgress(progressValue);
                        Thread.sleep(200);
                    }

                    loadComplete = true;
                }
            }catch(Exception ex)
            {
                Log.i(ASYNC_TASK_TAG, ex.getMessage());
            }finally {
                if(loadComplete) {
                    // Load complete display message.
                    retBuf.append("Load complete.");
                }else
                {
                    // Load cancel display message.
                    retBuf.append("Load canceled.");
                }
                return retBuf.toString();
            }
        }

        protected void onPostExecute(String result) {
            Log.i(ASYNC_TASK_TAG, "onPostExecute(" + result + ") is invoked.");
            // Show the result in log TextView object.
            asyncTaskLogTextView.setText(result);

            asyncTaskProgressBar.setProgress(100);

            executeAsyncTaskButton.setEnabled(true);
            cancelAsyncTaskButton.setEnabled(false);
        }


        protected void onProgressUpdate(Integer... values) {
            Log.i(ASYNC_TASK_TAG, "onProgressUpdate(" + values + ") is called");
            asyncTaskProgressBar.setProgress(values[0]);
            asyncTaskLogTextView.setText("loading..." + values[0] + "%");
        }

        protected void onCancelled(String result) {
            Log.i(ASYNC_TASK_TAG, "onCancelled(" + result + ") is invoked.");
            asyncTaskLogTextView.setText(result);
            asyncTaskProgressBar.setProgress(0);

            executeAsyncTaskButton.setEnabled(true);
            cancelAsyncTaskButton.setEnabled(false);
        }
    }

}
