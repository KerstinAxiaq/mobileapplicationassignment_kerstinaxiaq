package com.example.ccuapplication;


import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.os.ParcelFileDescriptor.MODE_APPEND;


/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment3 extends Fragment {

    private CoordinatorLayout coordinatorLayout;

    public Fragment3() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_fragment3, container, false);
        coordinatorLayout = view.findViewById(R.id.coordinatorLayout);
        return view;
    }

    Date date = new Date(System.currentTimeMillis()); //or simply new Date();

    @Override
    public void onStart() {
        super.onStart();
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(this.getActivity().getWindow().getContext());
        String myDate = sharedPreferences.getString("time", "default value");
        Snackbar snackbar= Snackbar.make(coordinatorLayout, "Data and time of last activity: "+myDate, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(this.getActivity().getWindow().getContext());
        String myDate = sharedPreferences.getString("time", "default value");
        Snackbar snackbar = Snackbar.make(coordinatorLayout, "Data and time of last activity: "+myDate, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    @Override
    public void onPause() {
        super.onPause();
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(this.getActivity().getWindow().getContext());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
        String strDate = formatter.format(date);
        editor.putString("time",strDate);
        editor.apply();
    }

    @Override
    public void onStop() {
        super.onStop();
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(this.getActivity().getWindow().getContext());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
        String strDate = formatter.format(date);
        editor.putString("time",strDate);
        editor.apply();
    }

}
